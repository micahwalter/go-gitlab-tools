# go-gitlab-tools

A few tools for doing things on GitLab

## create-projects

Creates new projects on GitLab for a given folder or projects and pushes repos to their new remotes

### Notes

* This assumes you have a folder full of Git repos.
* The script will create a new project on GitLab according to your settings
* It will then rename the remote origin in each repo's git/config
* Finally it will push the repo to its new remote origin

* You will need a GitLab private access token
* It currently requires a namespace ID.
* Your remote base should match the remote base and namespace you've set up on GitLab.*

### Use

``