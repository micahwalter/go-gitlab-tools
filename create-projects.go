package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strings"
)

func main() {

	// get some command line flags
	userID := flag.String("user-id", "", "Your GitLab User ID")
	token := flag.String("token", "", "You GitLab Private Access Token")
	namespaceID := flag.String("namespace-id", "", "A GitLab Namespace ID")
	visibility := flag.String("visibility", "private", "Sets project visibility")
	remoteBase := flag.String("remote-base", "", "Base for your remotes on GitLab")
	flag.Parse()

	// read in a list of folders from a given path

	files, err := ioutil.ReadDir("./")
	if err != nil {
		log.Fatal(err)
	}

	// loop through them and call the GitLab API to create a new project for each one if it isnt already created

	apiURL := "https://gitlab.com/api/v4/projects"

	for _, f := range files {
		if f.IsDir() {
			fmt.Println(f.Name())

			// create a project with this API call
			// POST /projects

			data := url.Values{}
			data.Set("name", f.Name())
			data.Add("user_id", *userID)
			data.Add("path", strings.ToLower(f.Name()))
			data.Add("namespace_id", *namespaceID)
			data.Add("private_token", *token)
			data.Add("visibility", *visibility)

			client := &http.Client{}
			r, _ := http.NewRequest("POST", apiURL, strings.NewReader(data.Encode()))

			resp, _ := client.Do(r)

			fmt.Println(resp.Status)

			// change to the working directory
			os.Chdir(strings.ToLower(f.Name()))

			// git remote rm origin
			// git push -u origin --tags

			cmd := exec.Command("git", "remote", "rm", "origin")
			stdout, err := cmd.Output()

			if err != nil {
				println(err.Error())
				return
			}

			print(string(stdout))

			remote := *remoteBase + strings.ToLower(f.Name()) + ".git"
			cmd = exec.Command("git", "remote", "add", "origin", remote)
			stdout, err = cmd.Output()

			if err != nil {
				println(err.Error())
				return
			}

			print(string(stdout))

			// git push -u origin --all

			cmd = exec.Command("git", "push", "-u", "origin", "--all")
			stdout, err = cmd.Output()

			if err != nil {
				println(err.Error())
				return
			}

			print(string(stdout))

			os.Chdir("..")

		}

	}

}
